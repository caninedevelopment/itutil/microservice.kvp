﻿// <copyright file="Kvpinfo.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using System;
using System.Collections.Generic;

namespace ITUtil.Kvp.V1.DTO
{
    /// <summary>
    /// KvpInfo
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class KvpInfo
    {
        /// <summary>
        /// Gets or sets key
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets value
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// gets or sets scope
        /// </summary>
        public string scope { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is KvpInfo info &&
                   this.key == info.key &&
                   this.value == info.value &&
                   this.scope == info.scope;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.key, this.value, this.scope);
        }
    }
}
