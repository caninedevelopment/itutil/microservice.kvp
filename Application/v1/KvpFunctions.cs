﻿// <copyright file="KvpFunctions.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Microservice.V1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using StackExchange.Redis;

    /// <summary>
    /// KvpFunctions
    /// </summary>
    public static class KvpFunctions
    {
        private static ITUtil.Kvp.V1.DTO.Settings settings = ITUtil.Kvp.V1.DTO.Settings.LoadSettings();
        private static string[] splitIpPort = settings.redisServerIp.Split(':');
        private static string ip = splitIpPort[0].ToString(ITUtil.Common.Constants.Culture.DefaultCulture);
        private static int port = Convert.ToInt32(splitIpPort[1], ITUtil.Common.Constants.Culture.DefaultCulture);

        private static ConnectionMultiplexer redisConn = ConnectionMultiplexer.Connect(settings.redisServerIp);
        private static IDatabase redDb = redisConn.GetDatabase();
        private static IServer server = redisConn.GetServer(ip, port);

        /// <summary>
        /// SetKvp
        /// </summary>
        /// <param name="kvpInfo">KvpInfo</param>
        /// <returns>bool</returns>
        public static bool SetKvp(ITUtil.Kvp.V1.DTO.KvpInfo kvpInfo)
        {
            if (kvpInfo == null)
            {
                throw new ArgumentNullException(nameof(kvpInfo));
            }

            if (kvpInfo.key == null || kvpInfo.key.Length == 0)
            {
                throw new ValidationException("No Key found");
            }

            if (kvpInfo.scope == null || kvpInfo.scope.Length == 0)
            {
                throw new ValidationException("No Scope found");
            }

            var newKey = kvpInfo.scope + "_" + kvpInfo.key;
            if (redDb.KeyExists(newKey))
            {
                return false;
            }
            else
            {
                redDb.StringSet(newKey, kvpInfo.value);
                return true;
            }
        }

        /// <summary>
        /// SetOrUpdateKvp
        /// </summary>
        /// <param name="kvpInfo">KvpInfo</param>
        /// <returns>bool</returns>
        public static bool SetOrUpdateKvp(ITUtil.Kvp.V1.DTO.KvpInfo kvpInfo)
        {
            if (kvpInfo == null)
            {
                throw new ArgumentNullException(nameof(kvpInfo));
            }

            if (kvpInfo.key == null || kvpInfo.key.Length == 0)
            {
                throw new ValidationException("No Key found");
            }

            if (kvpInfo.scope == null || kvpInfo.scope.Length == 0)
            {
                throw new ValidationException("No Scope found");
            }

            var newKey = kvpInfo.scope + "_" + kvpInfo.key;
            if (redDb.KeyExists(newKey))
            {
                redDb.KeyDelete(newKey);
            }

            redDb.StringSet(newKey, kvpInfo.value);

            return true;
        }

        /// <summary>
        /// GetKvp
        /// </summary>
        /// <param name="kvpInfo">KvpInfo</param>
        /// <returns>ITUtil.Kvp.V1.DTO.KvpInfo</returns>
        public static ITUtil.Kvp.V1.DTO.KvpInfo GetKvp(ITUtil.Kvp.V1.DTO.KvpInfo kvpInfo)
        {
            if (kvpInfo.key == null)
            {
                throw new ArgumentNullException(nameof(kvpInfo.key));
            }

            if (kvpInfo.scope == null)
            {
                throw new ArgumentNullException(nameof(kvpInfo.scope));
            }

            if (kvpInfo.key == null || kvpInfo.key.Length == 0)
            {
                throw new ValidationException("No Key found");
            }

            if (kvpInfo.scope == null || kvpInfo.scope.Length == 0)
            {
                throw new ValidationException("No Scope found");
            }

            var newKey = kvpInfo.scope + "_" + kvpInfo.key;
            if (redDb.KeyExists(newKey))
            {
                kvpInfo.value = redDb.StringGet(newKey);
                return kvpInfo;
            }
            else
            {
                return kvpInfo;
            }
        }

        /// <summary>
        /// GetList
        /// </summary>
        /// <param name="kvpinfo">KvpInfo</param>
        /// <returns>List ITUtil.Kvp.V1.DTO.KvpInfo </returns>
        public static List<ITUtil.Kvp.V1.DTO.KvpInfo> GetList(ITUtil.Kvp.V1.DTO.KvpInfo kvpinfo)
        {
            if (kvpinfo.scope == null)
            {
                throw new ArgumentNullException(nameof(kvpinfo.scope));
            }

            if (kvpinfo.scope == null || kvpinfo.scope.Trim().Length == 0)
            {
                throw new ValidationException("No Scope found");
            }

            var kvps = new List<ITUtil.Kvp.V1.DTO.KvpInfo>();
            var rediskeys = server.Keys(0, kvpinfo.scope + "*");
            foreach (var rediskey in rediskeys)
            {
                var kvp = new ITUtil.Kvp.V1.DTO.KvpInfo();
                var key = rediskey.ToString().Replace(kvpinfo.scope + "_", string.Empty, StringComparison.OrdinalIgnoreCase);
                kvp.key = key;
                kvp.scope = kvpinfo.scope;
                kvp.value = redDb.StringGet(rediskey);
                kvps.Add(kvp);
            }

            return kvps;
        }

        /// <summary>
        /// DeleteAll
        /// </summary>
        /// <param name="scope">string</param>
        /// <returns>bool</returns>
        public static bool DeleteAll(string scope)
        {
            if (scope == null || scope.Trim().Length == 0)
            {
                return false;
            }
            else
            {
                var keys = server.Keys(0, scope + "*").ToList();
                if (keys.Count > 0)
                {
                    foreach (var key in keys)
                    {
                        redDb.KeyDelete(key);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// DeleteOne
        /// </summary>
        /// <param name="kvpInfo">KvpInfo</param>
        /// <returns>bool</returns>
        public static bool DeleteOne(ITUtil.Kvp.V1.DTO.KvpInfo kvpInfo)
        {
            if (kvpInfo.key == null)
            {
                throw new ArgumentNullException(nameof(kvpInfo.key));
            }

            if (kvpInfo.scope == null)
            {
                throw new ArgumentNullException(nameof(kvpInfo.scope));
            }

            if (kvpInfo.scope == null || kvpInfo.scope.Trim().Length == 0)
            {
                return false;
            }

            if (kvpInfo.key == null || kvpInfo.key.Trim().Length == 0)
            {
                return false;
            }

            var newKey = kvpInfo.scope + "_" + kvpInfo.key;
            if (redDb.KeyExists(newKey))
            {
                redDb.KeyDelete(kvpInfo.scope + "_" + kvpInfo.key);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
