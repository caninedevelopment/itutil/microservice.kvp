﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Microservice.V1
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return "Kvp";
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        private const string Servicename = "Kvp";
        private static readonly MetaDataDefinition KvpClaim = new MetaDataDefinition(Servicename, "kvp", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "User is allowed to use Kvp"));
        private static readonly MetaDataDefinition AdminClaim = new MetaDataDefinition(Servicename, "Settings", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "User is allowed to setup kvp settings or see kvp settings"));

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "setSettings",
                       "Sets the settings for KVP",
                       typeof(ITUtil.Kvp.V1.DTO.Settings),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { AdminClaim }),
                       ExecuteOperationSetSettings),
                   new OperationDescription(
                       "getSettings",
                       "gets the settings",
                       null,
                       typeof(ITUtil.Kvp.V1.DTO.Settings),
                       new MetaDataDefinitions(new MetaDataDefinition[] { AdminClaim }),
                       ExecuteOperationGetSettings),
                   new OperationDescription(
                       "setKvp",
                       "updates redis with kvp value",
                       typeof(ITUtil.Kvp.V1.DTO.KvpInfo),
                       typeof(bool),
                       new MetaDataDefinitions(new MetaDataDefinition[] { KvpClaim }),
                       ExecuteOperationSetKvp),
                   new OperationDescription(
                       "setOrUpdateKvp",
                       "updates redis with kvp value",
                       typeof(ITUtil.Kvp.V1.DTO.KvpInfo),
                       typeof(bool),
                       new MetaDataDefinitions(new MetaDataDefinition[] { KvpClaim }),
                       ExecuteOperationSetOrUpdateKvp),
                   new OperationDescription(
                       "getKvp",
                       "gets a kvp from redis",
                       typeof(ITUtil.Kvp.V1.DTO.KvpInfo),
                       typeof(ITUtil.Kvp.V1.DTO.KvpInfo),
                       new MetaDataDefinitions(new MetaDataDefinition[] { KvpClaim }),
                       ExecuteOperationGetKvp),
                   new OperationDescription(
                       "getAll",
                       "gets all keys from redis",
                       null,
                       typeof(List<ITUtil.Kvp.V1.DTO.KvpInfo>),
                       new MetaDataDefinitions(new MetaDataDefinition[] { KvpClaim }),
                       ExecuteOperationGetAll),
                   new OperationDescription(
                       "deleteOne",
                       "delete key from redis",
                       typeof(ITUtil.Kvp.V1.DTO.KvpInfo),
                       typeof(bool),
                       new MetaDataDefinitions(new MetaDataDefinition[] { KvpClaim }),
                       ExecuteOperationDeleteOne),
                   new OperationDescription(
                       "deleteAll",
                       "delete all keys from redis",
                       typeof(ITUtil.Kvp.V1.DTO.KvpInfo),
                       typeof(bool),
                       new MetaDataDefinitions(new MetaDataDefinition[] { KvpClaim }),
                       ExecuteOperationDeleteAll),
                };
            }
        }

        /// <summary>
        /// ExecuteOperationSetSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSetSettings(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var newsettings = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.Settings>(wrapper.messageData);
            Kvp.V1.DTO.Settings.SaveSettings(newsettings);
            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationGetSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationGetSettings(MessageWrapper wrapper)
        {
            ITUtil.Kvp.V1.DTO.Settings settings = Kvp.V1.DTO.Settings.LoadSettings();
            return ReturnMessageWrapper.CreateResult(true, wrapper, settings, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationSetKvp
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSetKvp(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var kvpinfo = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.KvpInfo>(wrapper.messageData);
            var res = ITUtil.Microservice.V1.KvpFunctions.SetKvp(kvpinfo);
            if (res)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Key already exists"));
            }
        }

        /// <summary>
        /// ExecuteOperationSetOrUpdateKvp
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSetOrUpdateKvp(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var kvpinfo = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.KvpInfo>(wrapper.messageData);
            var res = ITUtil.Microservice.V1.KvpFunctions.SetOrUpdateKvp(kvpinfo);

            return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationGetKvp
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationGetKvp(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var kvpinfo = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.KvpInfo>(wrapper.messageData);
            var kvp = ITUtil.Microservice.V1.KvpFunctions.GetKvp(kvpinfo);
            if (kvp.value == null || kvp.value.Length > 0)
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Key not found"));
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, kvp, LocalizedString.OK);
            }
        }

        /// <summary>
        /// ExecuteOperationGetAll
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationGetAll(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var kvpinfo = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.KvpInfo>(wrapper.messageData);
            var listkvp = ITUtil.Microservice.V1.KvpFunctions.GetList(kvpinfo);
            if (listkvp[0].value == null)
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Keys not found"));
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, listkvp, LocalizedString.OK);
            }
        }

        /// <summary>
        /// ExecuteOperationDeleteAll
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationDeleteAll(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var kvpinfo = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.KvpInfo>(wrapper.messageData);
            var res = ITUtil.Microservice.V1.KvpFunctions.DeleteAll(kvpinfo.scope);
            if (res)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "No keys found to delete"));
            }
        }

        /// <summary>
        /// ExecuteOperationDeleteOne
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationDeleteOne(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var kvpinfo = MessageDataHelper.FromMessageData<ITUtil.Kvp.V1.DTO.KvpInfo>(wrapper.messageData);
            var res = ITUtil.Microservice.V1.KvpFunctions.DeleteOne(kvpinfo);
            if (res)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "No keys found to delete"));
            }
        }
    }
}
