﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using ITUtil.Kvp.V1.DTO;
    using ITUtil.Microservice.V1;
    using NUnit.Framework;
    using StackExchange.Redis;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        private static ConnectionMultiplexer redisConn = ConnectionMultiplexer.Connect("192.168.1.39:6379");
        private static IDatabase redDb = redisConn.GetDatabase();

        /// <summary>
        /// SaveRedisServerIp
        /// </summary>
        [Test]
        public void SaveRedisServerIp()
        {
            string gatewayvalue = "192.168.1.39:6379";
            Settings.SaveSettings(new Settings() { redisServerIp = gatewayvalue });
            var loaded = Settings.LoadSettings();
            Assert.AreEqual(gatewayvalue, loaded.redisServerIp, "Loaded settings should be the same as saved settings");
        }

        /// <summary>
        /// SetKvpWithNewKey
        /// </summary>
        [Test]
        public void SetKvpWithNewKey()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.key = "test1";
            kvp.scope = "unittest";
            kvp.value = "dette er en uinttest";
            var response = KvpFunctions.SetKvp(kvp);
            redDb.KeyDelete(kvp.scope + "_" + kvp.key);
            Assert.IsTrue(response);
        }

        /// <summary>
        /// SetKvpWithExistsKey
        /// </summary>
        [Test]
        public void SetKvpWithExistsKey()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.key = "test1";
            kvp.scope = "unittest";
            kvp.value = "dette er en uinttest";
            redDb.StringSet(kvp.scope + "_" + kvp.key, kvp.value);
            var response = KvpFunctions.SetKvp(kvp);
            redDb.KeyDelete(kvp.scope + "_" + kvp.key);
            Assert.IsFalse(response);
        }

        /// <summary>
        /// SetKvpWithNoKey
        /// </summary>
        [Test]
        public void SetKvpWithNoKey()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.scope = "unittest";
            kvp.value = "dette er en uinttest";
            var ex = Assert.Throws<ValidationException>(() => KvpFunctions.SetKvp(kvp));
            Assert.AreEqual("No Key found", ex.Message);
        }

        /// <summary>
        /// SetKvpWithNoScope
        /// </summary>
        [Test]
        public void SetKvpWithNoScope()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.key = "test1";
            kvp.value = "dette er en uinttest";
            var ex = Assert.Throws<ValidationException>(() => KvpFunctions.SetKvp(kvp));
            Assert.AreEqual("No Scope found", ex.Message);
        }

        /// <summary>
        /// GetKvpWithNoKey
        /// </summary>
        [Test]
        public void GetKvpWithNoKey()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.scope = "unittest";
            var ex = Assert.Throws<ValidationException>(() => KvpFunctions.GetKvp(kvp));
            Assert.AreEqual("No Key found", ex.Message);
        }

        /// <summary>
        /// GetKvpWithNoScope
        /// </summary>
        [Test]
        public void GetKvpWithNoScope()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.key = "test1";
            var ex = Assert.Throws<ValidationException>(() => KvpFunctions.GetKvp(kvp));
            Assert.AreEqual("No Scope found", ex.Message);
        }

        /// <summary>
        /// GetKvpThereExists
        /// </summary>
        [Test]
        public void GetKvpThereExists()
        {
            KvpInfo kvp = new KvpInfo();
            kvp.key = "test1";
            kvp.scope = "unittest";
            kvp.value = "dette er en uinttest";
            redDb.StringSet(kvp.scope + "_" + kvp.key, kvp.value);
            var res = KvpFunctions.GetKvp(kvp);
            redDb.KeyDelete(kvp.scope + "_" + kvp.key);
            Assert.AreEqual(res.key, kvp.key);
            Assert.AreEqual(res.scope, kvp.scope);
            Assert.AreEqual(res.value, kvp.value);
        }

        /// <summary>
        /// SetKvpGetList
        /// </summary>
        [Test]
        public void SetKvpGetList()
        {
            var kvplist = new List<KvpInfo>();
            for (int i = 0; i < 5; i++)
            {
                var kvp = new KvpInfo();
                kvp.key = "key" + i;
                kvp.value = "unittest value" + i;
                kvp.scope = "unittest";
                redDb.StringSet(kvp.scope + "_" + kvp.key, kvp.value);
                kvplist.Add(kvp);
            }

            var res = KvpFunctions.GetList(kvplist[0]);

            for (int i = 0; i < kvplist.Count; i++)
            {
                redDb.KeyDelete(kvplist[i].scope + "_" + kvplist[i].key);
            }

            Assert.AreEqual(res.Count, 5);
            Assert.IsTrue(res.Contains(kvplist[0]));
            Assert.IsTrue(res.Contains(kvplist[1]));
            Assert.IsTrue(res.Contains(kvplist[2]));
            Assert.IsTrue(res.Contains(kvplist[3]));
            Assert.IsTrue(res.Contains(kvplist[4]));
        }

        /// <summary>
        /// GetListKvpWithNoScope
        /// </summary>
        [Test]
        public void GetListKvpWithNoScope()
        {
            KvpInfo kvp = new KvpInfo();
            var ex = Assert.Throws<ValidationException>(() => KvpFunctions.GetList(kvp));
            Assert.AreEqual("No Scope found", ex.Message);
        }

        /// <summary>
        /// DeleteOneKvp
        /// </summary>
        [Test]
        public void DeleteOneKvp()
        {
            var kvpInfo = new KvpInfo();
            kvpInfo.key = "key";
            kvpInfo.value = "unittest value";
            kvpInfo.scope = "unittest";
            redDb.StringSet(kvpInfo.scope + "_" + kvpInfo.key, kvpInfo.value);
            Assert.IsTrue(KvpFunctions.DeleteOne(kvpInfo));
        }

        /// <summary>
        /// DeleteOneThereDontExists
        /// </summary>
        [Test]
        public void DeleteOneThereDontExists()
        {
            var kvpInfo = new KvpInfo();
            kvpInfo.key = "key";
            kvpInfo.value = "unittest value";
            kvpInfo.scope = "unittest";
            Assert.IsFalse(KvpFunctions.DeleteOne(kvpInfo));
        }

        /// <summary>
        /// DeleteOneWithNoKey
        /// </summary>
        [Test]
        public void DeleteOneWithNoKey()
        {
            var kvpInfo = new KvpInfo();
            kvpInfo.value = "unittest value";
            kvpInfo.scope = "unittest";
            Assert.IsFalse(KvpFunctions.DeleteOne(kvpInfo));
        }

        /// <summary>
        /// DeleteOneWithNoScope
        /// </summary>
        [Test]
        public void DeleteOneWithNoScope()
        {
            var kvpInfo = new KvpInfo();
            kvpInfo.key = "key";
            kvpInfo.value = "unittest value";
            Assert.IsFalse(KvpFunctions.DeleteOne(kvpInfo));
        }

        /// <summary>
        /// DeleteAllKvp
        /// </summary>
        [Test]
        public void DeleteAllKvp()
        {
            var kvpInfo = new KvpInfo();
            kvpInfo.scope = "unittest";
            for (int i = 0; i < 5; i++)
            {
                var kvp = new KvpInfo();
                kvp.key = "key" + i;
                kvp.value = "unittest value" + i;
                kvp.scope = "unittest";
                redDb.StringSet(kvp.scope + "_" + kvp.key, kvp.value);
            }

            Assert.IsTrue(KvpFunctions.DeleteAll("unittest"));
            Assert.IsEmpty(KvpFunctions.GetList(kvpInfo));
        }
    }
}
